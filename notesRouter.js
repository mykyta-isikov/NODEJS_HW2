const express = require('express');

const router = express.Router();
const {
  getNotes, createNote, getNote, editNote, toggleNoteCheck, delNote,
} = require('./notesService');
const { isNoteExist } = require('./middleware/isNoteExist');

router.get('/', getNotes);

router.post('/', createNote);

router.get('/:id', isNoteExist, getNote);

router.put('/:id', isNoteExist, editNote);

router.patch('/:id', isNoteExist, toggleNoteCheck);

router.delete('/:id', isNoteExist, delNote);

module.exports = {
  notesRouter: router,
};
