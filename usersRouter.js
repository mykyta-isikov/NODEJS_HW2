const express = require('express');

const router = express.Router();
const { getProfile, delProfile, changePassword } = require('./usersService');
const { validateChangePasswordInput } = require('./middleware/validateChangePasswordInput');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/me', authMiddleware, getProfile);

router.delete('/me', authMiddleware, delProfile);

router.patch('/me', authMiddleware, validateChangePasswordInput, changePassword);

module.exports = {
  usersRouter: router,
};
