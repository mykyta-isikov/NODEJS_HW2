const { Note } = require('./models/Notes');

const getNotes = async (req, res) => {
  const [offset, limit] = [req.query.offset || 0, req.query.limit || 0];
  Note.find({ userId: req.user.userId })
    .then((notes) => {
      const selectionLimit = limit <= 0 ? notes.length : offset + limit;
      const desiredNotes = notes.slice(req.query.offset, selectionLimit);
      const output = {
        offset,
        limit,
        count: desiredNotes.length,
        notes: desiredNotes,
      };
      res.status(200).send(output);
    }).catch((err) => {
      res.status(500).send({ message: err });
    });
};

const createNote = async (req, res) => {
  if (typeof req.body.text !== 'string') {
    res.status(400).send({ message: 'Invalid input' });
    return;
  }

  const note = new Note({
    userId: req.user.userId,
    text: req.body.text,
  });

  note.save()
    .then(() => {
      res.status(200).send({ message: 'Success' });
    }).catch((err) => {
      res.status(500).send({ message: err });
    });
};

const getNote = async (req, res) => {
  Note.findById(req.params.id)
    .then((note) => {
      const output = {
        note: {
          /* eslint-disable-next-line no-underscore-dangle */
          _id: note._id.toString(),
          userId: note.userId,
          completed: note.completed,
          createdDate: note.createdDate,
          text: note.text,
        },
      };
      res.status(200).send(output);
    });
};

const editNote = async (req, res) => {
  if (typeof req.body.text !== 'string') {
    res.status(400).send({ message: 'Invalid input' });
  }

  const note = await Note.findById(req.params.id);
  note.text = req.body.text;
  note.save()
    .then(() => {
      res.status(200).send({ message: 'Success' });
    }).catch((err) => {
      res.status(500).send({ message: err });
    });
};

const toggleNoteCheck = async (req, res) => {
  const note = await Note.findById(req.params.id);
  note.completed = !note.completed;
  note.save()
    .then(() => {
      res.status(200).send({ message: 'Success' });
    }).catch((err) => {
      res.status(500).send({ message: err });
    });
};

const delNote = async (req, res) => {
  Note.findByIdAndDelete(req.params.id)
    .then(() => {
      res.status(200).send({ message: 'Success' });
    });
};

module.exports = {
  getNotes,
  createNote,
  getNote,
  editNote,
  toggleNoteCheck,
  delNote,
};
