const fs = require('fs');
const bodyParser = require('body-parser');
const express = require('express');
const morgan = require('morgan');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const cors = require('cors');

const { usersRouter } = require('./usersRouter');
const { notesRouter } = require('./notesRouter');
const { authRouter } = require('./authRouter');
const { authMiddleware } = require('./middleware/authMiddleware');
const { uiRouter } = require('./uiRouter');

const app = express();
dotenv.config();
mongoose.connect(process.env.DB_CONNECTION_STRING);

app.use(cors());
app.use(express.static('public'));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.use(morgan('tiny', { stream: fs.createWriteStream('./requests.log', { flags: 'a' }) }));

app.use('/api/users', usersRouter);
app.use('/api/notes', authMiddleware, notesRouter);
app.use('/api/auth', authRouter);
app.use('/site', uiRouter);

app.get('/', (req, res) => {
  res.status(200).send({ message: 'Success' });
});

const PORT = process.env.PORT || 8080;

app.listen(PORT);
