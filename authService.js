const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/Users');

const register = async (req, res) => {
  const { username, password } = req.body;
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  try {
    const duplicate = await User.findOne({ username });
    if (duplicate !== null) {
      res.status(400).send({ message: 'This user already exists' });
      return;
    }
  } catch (err) {
    res.status(500).send({ message: err });
    return;
  }

  user.save()
    .then(() => {
      res.status(200).send({ message: 'Success' });
    })
    .catch((err) => {
      res.status(500).send({ message: err });
    });
};

const login = async (req, res) => {
  try {
    const user = await User.findOne({ username: req.body.username });
    if (user === null) {
      res.status(400).send({ message: 'Not authorized' });
      return;
    }
    if (await bcrypt.compare(req.body.password, user.password) === false) {
      res.status(400).send({ message: 'Not authorized' });
      return;
    }
    const payload = {
      username: user.username,
      password: user.password,
      /* eslint-disable-next-line no-underscore-dangle */
      userId: user._id,
    };
    const authToken = jwt.sign(payload, process.env.JWT_PASSWORD);
    const output = {
      message: 'Success',
      jwt_token: authToken,
    };
    res.status(200).send(output);
  } catch (err) {
    res.status(500).send({ message: err });
  }
};

module.exports = {
  register,
  login,
};
