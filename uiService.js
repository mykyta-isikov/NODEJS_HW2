const site = async (req, res) => {
  res.status(200).render('site.ejs');
};

module.exports = {
  site,
};
