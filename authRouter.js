const express = require('express');

const router = express.Router();
const { register, login } = require('./authService');
const { validateAuthInput } = require('./middleware/validateAuthInput');

router.post('/register', validateAuthInput, register);

router.post('/login', validateAuthInput, login);

module.exports = {
  authRouter: router,
};
