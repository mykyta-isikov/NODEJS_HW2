const express = require('express');
const {
  site,
} = require('./uiService');

const router = express.Router();

router.get('/', site);

module.exports = {
  uiRouter: router,
};
