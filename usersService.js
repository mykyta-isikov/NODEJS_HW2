const bcrypt = require('bcryptjs');

const { User } = require('./models/Users');

const getProfile = async (req, res) => {
  try {
    const user = await User.findById(req.user.userId);

    const output = {
      user: {
        /* eslint-disable-next-line no-underscore-dangle */
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      },
    };
    res.status(200).send(output);
  } catch (err) {
    res.status(500).send({ message: err });
  }
};

const delProfile = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.user.userId);
    res.status(200).send({ message: 'Success' });
  } catch (err) {
    res.status(500).send({ message: err });
  }
};

const changePassword = async (req, res) => {
  try {
    const user = await User.findById(req.user.userId);

    if (await bcrypt.compare(req.body.oldPassword, user.password) === false) {
      res.status(400).send({ message: 'Old password is incorrect' });
      return;
    }

    user.password = await bcrypt.hash(req.body.newPassword, 10);

    await user.save();

    res.status(200).send({ message: 'Success' });
  } catch (err) {
    res.status(500).send({ message: err });
  }
};

module.exports = {
  getProfile,
  delProfile,
  changePassword,
};
