function toggleAuthUI() {
  if (auth.jwt_token) {
    authorizedElements.forEach((elem) => {
      elem.classList.remove('hidden');
    });
    unauthorizedElements.forEach((elem) => {
      elem.classList.add('hidden');
    });
  } else {
    authorizedElements.forEach((elem) => {
      elem.classList.add('hidden');
    });
    unauthorizedElements.forEach((elem) => {
      elem.classList.remove('hidden');
    });
  }
}

function userLogout() {
  auth = {};
  document.cookie = 'auth=; expires=Thu, 01 Jan 1970 00:00:00 UTC;';
  toggleAuthUI();
}

async function userLogin(username, password) {
  requestData.method = 'POST';
  requestData.body = JSON.stringify({
    username,
    password,
  });
  const url = `${document.location.origin}/api/auth/login`;
  const result = await fetch(url, requestData);
  if (result.ok) {
    const data = await result.json();
    auth.jwt_token = data.jwt_token;
    document.cookie = `auth=${JSON.stringify(auth)}`;
    toggleAuthUI();
  }
}

async function createNewNote(text, reqData) {
  reqData.method = 'POST';
  reqData.body = JSON.stringify({ text });
  const result = await fetch(getNotesUrl, reqData);
  reqData.body = null;
}

async function registerUser(username, password, reqData) {
  reqData.method = 'POST';
  reqData.body = JSON.stringify({
    username,
    password,
  });
  const url = `${document.location.origin}/api/auth/register`;
  await fetch(url, reqData);
}

async function getUser(reqData) {
  reqData.method = 'GET';
  const url = `${document.location.origin}/api/users/me`;
  const result = await fetch(url, reqData);
  return await result.json();
}

async function changeUserPassword(oldPassword, newPassword, reqData) {
  reqData.method = 'PATCH';
  const url = `${document.location.origin}/api/users/me`;
  reqData.body = JSON.stringify({
    oldPassword,
    newPassword,
  });
  const result = await fetch(url, reqData);
  reqData.body = null;
  return await result.json();
}

async function deleteUser(reqData) {
  reqData.method = 'DELETE';
  const url = `${document.location.origin}/api/users/me`;
  await fetch(url, reqData);
}

async function toggleNoteCheck(id, reqData) {
  reqData.method = 'PATCH';
  const url = `${getNotesUrl}/${id}`;
  await fetch(url, reqData);
}

async function deleteNote(id, reqData) {
  reqData.method = 'DELETE';
  const url = `${getNotesUrl}/${id}`;
  await fetch(url, reqData);
}

async function editNote(id, text, reqData) {
  reqData.method = 'PUT';
  const url = `${getNotesUrl}/${id}`;
  reqData.body = JSON.stringify({
    text,
  });
  await fetch(url, reqData);
  reqData.body = null;
}
