const loginPopupBtn = document.querySelector('#log-in');
const loginPopup = document.querySelector('#login-popup');
const loginCloseBtn = document.querySelector('#login-close');
const loginUsername = document.querySelector('#login-username');
const loginPassword = document.querySelector('#login-password');
const loginBtn = document.querySelector('#login-submit');

loginPopupBtn.addEventListener('click', async () => {
  loginPopup.classList.remove('hidden');
});

loginCloseBtn.addEventListener('click', async () => {
  loginPopup.classList.add('hidden');
});

loginBtn.addEventListener('click', async (event) => {
  event.target.disabled = true;
  event.target.innerText = '...';
  await userLogin(loginUsername.value, loginPassword.value, requestData);
  loginUsername.value = '';
  loginPassword.value = '';
  event.target.disabled = false;
  event.target.innerText = 'Log in';
  loginPopup.classList.add('hidden');
  await populateNotesContainer();
});
