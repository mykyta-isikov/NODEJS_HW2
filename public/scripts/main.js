const getNotesBtn = document.querySelector('#refresh');

getNotesBtn.addEventListener('click', async (event) => {
  event.target.disabled = true;
  event.target.innerText = '...';
  await populateNotesContainer();
  event.target.disabled = false;
  event.target.innerText = 'Refresh notes';
});
