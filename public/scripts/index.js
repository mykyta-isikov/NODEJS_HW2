const authorizedElements = document.querySelectorAll('.show-on-auth');
const unauthorizedElements = document.querySelectorAll('.hide-on-auth');

const auth = document.cookie ? JSON.parse(document.cookie.split('auth=')[1]) : {};

const getNotesUrl = `${document.location.origin}/api/notes`;
const requestData = {
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};

toggleAuthUI();
if (auth.jwt_token) {
  requestData.headers.authorization = `JWT ${auth.jwt_token}`;
  populateNotesContainer(requestData);
}

async function populateNotesContainer() {
  requestData.body = null;
  const noteContainer = document.querySelector('#notes-container');
  const noteTemplate = document.querySelector('#note-template');

  requestData.method = 'GET';
  requestData.headers.authorization = `JWT ${auth.jwt_token}`;

  const notes = await fetch(getNotesUrl, requestData);
  const data = await notes.json();
  noteContainer.innerText = '';
  for (const note of data.notes) {
    const clone = noteTemplate.content.cloneNode(true);
    const editForm = clone.querySelector('.edit-note-form');
    const noteContent = clone.querySelector('.note-content');
    const noteContentInput = clone.querySelector('.note-content-input');
    const checkNoteBtn = clone.querySelector('.check-note-btn');
    const deleteNoteBtn = clone.querySelector('.delete-note-btn');
    const editNoteBtn = clone.querySelector('.edit-note-btn');
    const editNoteSubmitBtn = clone.querySelector('.edit-note-submit-btn');

    clone.querySelector('.note').dataset.id = note._id;
    noteContent.innerText = note.text;
    clone.querySelector('.note-content-input').value = note.text;
    clone.querySelector('.note-checkbox').checked = note.completed;

    checkNoteBtn.addEventListener('click', async (event) => {
      event.target.disabled = true;
      event.target.innerText = '...';
      await toggleNoteCheck(note._id, requestData);
      event.target.disabled = false;
      event.target.innerText = '✓';
      await populateNotesContainer();
    });

    deleteNoteBtn.addEventListener('click', async (event) => {
      event.target.disabled = true;
      event.target.innerText = '...';
      await deleteNote(note._id, requestData);
      event.target.disabled = false;
      event.target.innerText = '⨯';
      await populateNotesContainer();
    });

    editNoteBtn.addEventListener('click', async () => {
      editForm.classList.toggle('hidden');
      noteContent.classList.toggle('hidden');
    });

    editNoteSubmitBtn.addEventListener('click', async (event) => {
      event.target.disabled = true;
      event.target.innerText = '...';
      await editNote(note._id, noteContentInput.value, requestData);
      await populateNotesContainer();
    });

    noteContainer.appendChild(clone);
  }
}
