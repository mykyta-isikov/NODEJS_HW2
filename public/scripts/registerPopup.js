const registerPopupBtn = document.querySelector('#register');
const registerPopup = document.querySelector('#register-popup');
const registerCloseBtn = document.querySelector('#register-close');
const registerUsername = document.querySelector('#register-username');
const registerPassword = document.querySelector('#register-password');
const registerBtn = document.querySelector('#register-submit');

registerPopupBtn.addEventListener('click', async () => {
  registerPopup.classList.remove('hidden');
});

registerCloseBtn.addEventListener('click', async () => {
  registerPopup.classList.add('hidden');
});

registerBtn.addEventListener('click', async (event) => {
  event.target.disabled = true;
  event.target.innerText = '...';
  await registerUser(registerUsername.value, registerPassword.value, requestData);
  registerUsername.value = '';
  registerPassword.value = '';
  event.target.disabled = false;
  event.target.innerText = 'Create account';
  registerPopup.classList.add('hidden');
});
