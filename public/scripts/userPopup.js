const userPopupBtn = document.querySelector('#user');
const userPopup = document.querySelector('#user-popup');
const userCloseBtn = document.querySelector('#user-close');
const userUsername = document.querySelector('#user-username');
const userDate = document.querySelector('#user-date');
const userOldPassword = document.querySelector('#user-old-password');
const userNewPassword = document.querySelector('#user-new-password');
const userChangePwdBtn = document.querySelector('#user-change-pwd');
const userDeleteBtn = document.querySelector('#user-delete');

userPopupBtn.addEventListener('click', async (event) => {
  event.target.disabled = true;
  event.target.innerText = '...';

  if (!auth.user) {
    const data = await getUser(requestData);
    auth.user = {
      _id: data.user._id,
      username: data.user.username,
      createdDate: data.user.createdDate,
    };
  }
  userUsername.innerText = auth.user.username;
  userDate.innerText = new Date(auth.user.createdDate).toString().split(' GMT')[0];

  userPopup.classList.remove('hidden');
  event.target.disabled = false;
  event.target.innerText = 'User settings';
});

userCloseBtn.addEventListener('click', async () => {
  userPopup.classList.add('hidden');
});

userChangePwdBtn.addEventListener('click', async (event) => {
  event.target.disabled = true;
  event.target.innerText = '...';

  await changeUserPassword(userOldPassword.value, userNewPassword.value, requestData);
  userOldPassword.value = '';
  userNewPassword.value = '';

  event.target.disabled = false;
  event.target.innerText = 'Change password';
  registerPopup.classList.add('hidden');
});

userDeleteBtn.addEventListener('click', async (event) => {
  event.target.disabled = true;
  event.target.innerText = '...';

  await deleteUser(requestData);
  await userLogout();

  event.target.disabled = false;
  event.target.innerText = 'Delete account';
  userPopup.classList.add('hidden');
});
