const createNoteBtn = document.querySelector('#add-note');
const newNoteForm = document.querySelector('#note-popup');
const newNoteCloseBtn = document.querySelector('#new-note-close');
const newNoteTextarea = document.querySelector('#new-note-text');
const newNoteBtn = document.querySelector('#new-note-submit');

createNoteBtn.addEventListener('click', async () => {
  newNoteForm.classList.remove('hidden');
});

newNoteCloseBtn.addEventListener('click', async () => {
  newNoteForm.classList.add('hidden');
});

newNoteBtn.addEventListener('click', async (event) => {
  event.target.disabled = true;
  event.target.innerText = '...';
  await createNewNote(newNoteTextarea.value, requestData);
  newNoteTextarea.value = '';
  event.target.disabled = false;
  event.target.innerText = 'Create';
  newNoteForm.classList.add('hidden');
  await populateNotesContainer();
});
