const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: true,
  },

});

const Note = mongoose.model('Note', noteSchema);

module.exports = {
  Note,
};
