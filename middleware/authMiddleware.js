const jwt = require('jsonwebtoken');

const authMiddleware = async (req, res, next) => {
  const {
    authorization,
  } = req.headers;

  if (!authorization) {
    res.status(401).json({ message: 'Please, provide authorization header' });
    return;
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    res.status(401).json({ message: 'Please, include token to request' });
    return;
  }

  try {
    const tokenPayload = jwt.verify(token, process.env.JWT_PASSWORD);
    req.user = {
      userId: tokenPayload.userId,
      username: tokenPayload.username,
      password: tokenPayload.password,
    };
    next();
  } catch (err) {
    res.status(401).json({ message: err.message });
  }
};

module.exports = {
  authMiddleware,
};
