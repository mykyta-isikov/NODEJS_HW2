const { Note } = require('../models/Notes');

const isNoteExist = async (req, res, next) => {
  try {
    const note = await Note.findById(req.params.id);

    if (note === null) {
      res.status(400).send({ message: 'This note doesn\'t exist' });
      return;
    }

    next();
  } catch (err) {
    res.status(500).send({ message: err });
  }
};

module.exports = {
  isNoteExist,
};
