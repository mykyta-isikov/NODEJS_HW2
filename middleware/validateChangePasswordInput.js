const validateChangePasswordInput = (req, res, next) => {
  if (typeof req.body.oldPassword !== 'string' || typeof req.body.newPassword !== 'string') {
    res.status(400).send({ message: 'Invalid input' });
    return;
  }
  next();
};

module.exports = { validateChangePasswordInput };
