const validateAuthInput = (req, res, next) => {
  if (typeof req.body.username === 'string' && typeof req.body.password === 'string') {
    next();
  } else {
    res.status(400).send({ message: 'Invalid input' });
  }
};

module.exports = { validateAuthInput };
